import { Service } from "typedi";
import ProjectRepository from "../adapters/repositories/project_repository";
import Project from "../entities/project";
import User from "../entities/user";

@Service()
class CreateProject {
    constructor(
        private projectRepository: ProjectRepository
    ) {}

    call({user, projectName}: CreateProjectParameters) {
        const newProject = new Project({name: projectName, creator: user});

        this.projectRepository.addProject(newProject);
    }
}

interface CreateProjectParameters {
    user: User;
    projectName: string;
}