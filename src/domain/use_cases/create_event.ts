import { Service } from "typedi";
import Project from "../entities/project";
import Event from "../entities/event";
import Client from "../entities/client";
import Intent from "../entities/intent";
import Constraint from "../entities/constraint";
import Expectation from "../entities/expectation";

@Service()
class CreateEvent {
    constructor() {}

    call({project, client, intent, constraints, expectations}: CreateEventParameters) {
        const newClient = new Client({name: client});
        const newIntent = new Intent({description: intent});
        const newConstraints = constraints.map(constraint => new Constraint({description: constraint}));
        const newExpectations = expectations.map(expectation => new Expectation({description: expectation}));

        const newEvent = new Event({client: newClient, intent: newIntent, constraints: newConstraints, expectations: newExpectations});

        project.addEvent(newEvent);
    }
}

interface CreateEventParameters {
    project: Project;
    client: string;
    intent: string;
    constraints : Array<string>;
    expectations: Array<string>;
}