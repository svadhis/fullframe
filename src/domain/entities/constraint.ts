export default class Constraint {
    description: string;

    constructor({description}: ConstraintParameters) {
        this.description = description;
    }

}

interface ConstraintParameters {
    description: string;
}