import User from './user';
import Event from './event';

export default class Project {
    id: number | undefined;
    name: string;
    creator: User;
    events: Array<Event>;

    constructor({name, creator, id, events}: ProjectParameters) {
        this.name = name;
        this.creator = creator;
        this.id = id;
        this.events = events ?? [];
    }

    addEvent(event: Event) {
        this.events.push(event);
    }
}

interface ProjectParameters {
    name: string;
    creator: User;
    id?: number;
    events?: Array<Event>;
}