export default class Expectation {
    description: string;

    constructor({description}: ExpectationParameters) {
        this.description = description;
    }

}

interface ExpectationParameters {
    description: string;
}