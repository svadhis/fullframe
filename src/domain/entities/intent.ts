export default class Intent {
    description: string;

    constructor({description}: IntentParameters) {
        this.description = description;
    }

}

interface IntentParameters {
    description: string;
}