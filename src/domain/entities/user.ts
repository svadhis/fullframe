export default class User {
    id: number | undefined;

    constructor({id}: UserParameters) {
        this.id = id;
    }

}

interface UserParameters {
    id?: number;
}