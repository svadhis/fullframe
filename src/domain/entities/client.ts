export default class Client {
    name: string;

    constructor({name}: ClientParameters) {
        this.name = name;
    }

}

interface ClientParameters {
    name: string;
}