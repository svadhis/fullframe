import Client from "./client";
import Constraint from "./constraint";
import Expectation from "./expectation";
import Intent from "./intent";

export default class Event {
    client: Client;
    intent: Intent;
    constraints: Array<Constraint>;
    expectations: Array<Expectation>;

    constructor({client, intent, constraints, expectations}: EventParameters) {
        this.client = client;
        this.intent = intent;
        this.constraints = constraints;
        this.expectations = expectations;
    }

}

interface EventParameters {
    client: Client;
    intent: Intent;
    constraints: Array<Constraint>;
    expectations: Array<Expectation>;
}